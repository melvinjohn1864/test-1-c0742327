
//Name : Melvin John
//Student Id : C0742327


import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class BlazeDemoTest {
	
	//Constants
	WebDriver driver;
		
	final String CHROME_DRIVER_LOCATION = "/Users/owner/Desktop/chromedriver";
	
	final String baseUrl = "http://blazedemo.com/";

	@Before
	public void setUp() throws Exception {
		// Selenium setup
		
		System.setProperty("webdriver.chrome.driver",CHROME_DRIVER_LOCATION);
		driver = new ChromeDriver();
				
		// 3. Open Chrome and go to the base url;
		driver.get(baseUrl);
	}

	@After
	public void tearDown() throws Exception {
		
		// 7. Close the browser
		Thread.sleep(3000);
		driver.close();
	}

	@Test
	public void testForDepartureCities() {
		//1. Get all the departure cities in website
		
		
		WebElement departureCities = driver.findElement(By.name("fromPort"));
		List<WebElement> cities = departureCities.findElements(By.cssSelector("option"));
				
		System.out.println("Number of departure cities: " + cities.size());
		
		assertEquals(7, cities.size());
	}
	
	@Test
	public void testForVirginAmericaFlight() {
		
		
		//WebElement findFlightButton = driver.findElement(By.cssSelector("input.btn btn-primary"));
		
		//findFlightButton.click();
		
		


	}

}
