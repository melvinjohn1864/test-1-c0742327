
//Name : Melvin John
//Student Id : C0742327

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class SnakeTest {
	
	Snake snake1;
	Snake snake2;

	@Before
	public void setUp() throws Exception {
		
		//Create all the snakes which have to be used in test cases
		
		snake1 = new Snake("Peter",10,"coffee");
		snake2 = new Snake("Takis",80,"vegetables");
	}

	@After
	public void tearDown() throws Exception {
	}



	
	@Test
	public void testIsSnakeHealthy() {
		//Testing the is healthy function whether snake is healthy or unhealthy
		
		//Checking snake is healthy
		
		//1. Check  the favorite food is healthy
		Boolean isHealthyFood = snake2.isHealthy();
		
		// 2. Check isFavoriteFood = true
		assertEquals(true, isHealthyFood);
	
	}
	
	@Test
	public void testIsSnakeUnHealthy() {
		//Testing the is healthy function whether snake is healthy or unhealthy
		
		//Checking snake is not healthy
		
		//1.Check  the favorite food  is unhealthy
		Boolean isUnhealthyFood = snake1.isHealthy();
		
		// 2. Check isFavoriteFood = false
		assertEquals(false, isUnhealthyFood);
	
	}
	
	@Test
	public void testIsSnakeShorterThanCage() {
		//Testing if the snake length is shorter than cage length
		//cagelength > snakelength
		
		//1.Check  the length of cage with snake length
		Boolean isSnakeLengthShort = snake2.fitsInCage(90);
		
		// 2. Check isSnakeLengthShort = true
		assertEquals(true, isSnakeLengthShort);
	
	}
	
	@Test
	public void testIsSnakeEqualsCage() {
		//Testing if the snake length is equal to cage length
		//cagelength = snakelength
		
		//1.Check  the length of cage with snake length
		Boolean isSnakeLengthEquals = snake2.fitsInCage(80);
		
		// 2. Check isSnakeLengthEquals = false
		assertEquals(false, isSnakeLengthEquals);
	
	}
	
	@Test
	public void testIsSnakeLargerThanCage() {
		//Testing if the snake length is larger than cage length
		//cagelength < snakelength
		
		//1.Check  the length of cage with snake length
		Boolean isSnakeLengthLong = snake2.fitsInCage(70);
		
		// 2. Check isSnakeLengthLong = false
		assertEquals(false, isSnakeLengthLong);
	
	}

}
